package com.example.android.courtcounter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public int scoreCountForTeamA = 0;
    public int scoreCountForTeamB = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayForTeamA(0);
        displayForTeamB(0);
    }

    /**
     * Displays the given score for Team A.
     */
    public void displayForTeamA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_a_score);
        scoreView.setText(String.valueOf(score));
    }
    // Increase A team score three points
    public void upThreeButtonForTeamA(View view) {
        scoreCountForTeamA += 3;
        displayForTeamA(scoreCountForTeamA);
    }
    // Increase A team score two points
    public void upTwoButtonForTeamA(View view) {
        scoreCountForTeamA += 2;
        displayForTeamA(scoreCountForTeamA);
    }
    // Increase A team score one point
    public void freeThrowButtonForTeamA(View view) {
        scoreCountForTeamA += 1;
        displayForTeamA(scoreCountForTeamA);
    }


    /**
     * Displays the given score for Team B.
     */
    public void displayForTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_b_score);
        scoreView.setText(String.valueOf(score));
    }
    // Increase B team score three points
    public void upThreeButtonForTeamB(View view) {
        scoreCountForTeamB += 3;
        displayForTeamB(scoreCountForTeamB);
    }
    // Increase B team score two points
    public void upTwoButtonForTeamB(View view) {
        scoreCountForTeamB += 2;
        displayForTeamB(scoreCountForTeamB);
    }
    // Increase B team score one point
    public void freeThrowButtonForTeamB(View view) {
        scoreCountForTeamB += 1;
        displayForTeamB(scoreCountForTeamB);
    }
    // Erase teams score
    public void resetScoreButton(View view){
        scoreCountForTeamA = 0;
        scoreCountForTeamB = 0;
        displayForTeamA(scoreCountForTeamA);
        displayForTeamB(scoreCountForTeamB);
    }
}
